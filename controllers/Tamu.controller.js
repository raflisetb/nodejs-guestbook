// require model
const tamuModel = require ('../models/tamuModel'); 
const table = tamuModel.table;
// create an object 
var TamuController = {} ; 

TamuController.index = function (req, res)
{
	req.getConnection(function (err, connect)
	{
		var query = connect.query ('SELECT * FROM',table);
		if (err)
		{
			console.log('error %s', err); 
		}; 

		res.render('../views/tamu')	;
	});
} 

TamuController.tes = "select * from "  ; 

// export module 
module.exports = TamuController; 